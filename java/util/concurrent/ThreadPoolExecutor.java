package java.util.concurrent;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.*;

/**
 * 一个{@link ExecutorService}，它使用多个共享线程中的一个来执行每个提交的任务, 通常使用{@link Executors}工厂方法配置.
 *
 * <p>线程池解决了两个不同的问题: 由于减少了每个任务的调用开销，它们通常在执行大量异步任务时提供改进的性能。,
 * 并且它们提供了一种限制和管理资源的方法,
 * 包括线程，在执行任务集合时消耗.
 * 每个{@code ThreadPoolExecutor}还维护一些基本的统计信息，例如已完成任务的数量.
 *
 * <p>为了在广泛的上下文中有用，此类提供了许多可调整的参数和可扩展性钩子.
 * 但是，敦促程序员使用更方便的*{@link Executors}工厂方法{@link  Executors＃newCachedThreadPool}（无限制线程池）,
 * 带自动线程回收), {@link Executors＃newFixedThreadPool} （固定大小的线程池）和{@link  Executors＃newSingleThreadExecutor}（单个后台线程）,
 * 预配置最常用的设置场景. 否则，在手动配置和调整此类时，请使用以下指南:
 *
 * <dl>
 *
 * <dt>核心和最大池大小</dt>
 *
 * <dd>{@code ThreadPoolExecutor}将根据corePoolSize（请参阅{@link #getCorePoolSize}）和maximumPoolSize（请参阅{@link #getMaximumPoolSize）
 * 设置的界限来自动调整池大小（请参阅{@link #getPoolSize}） }）.
 *
 * 在方法{@link #execute（Runnable）}中提交新任务时,
 * 并且少于corePoolSize线程正在运行，创建一个新线程来处理请求, 即使其他工作线程处于空闲状态.
 * 如果有更多的corePoolSize但少于 maximumPoolSize线程正在运行, 仅当队列已满时才创建一个新线程.  通过设置corePoolSize和maximumPoolSize 相同,
 * 您创建一个固定大小的线程池. 通过将 maximumPoolSize设置为本质上不受限制的值，例如{@code  Integer.MAX_VALUE},
 * 您允许池容纳任意数量的并发任务. 最典型的是，核心池和最大池大小仅在构造时设置, 但也可以使用{@link #setCorePoolSize}和{@link  #setMaximumPoolSize}动态更改它们. </dd>
 *
 * <dt>按需施工</dt>
 *
 * <dd>默认情况下，甚至最初创建核心线程并仅在有新任务到达时才启动,
 * 但这可以使用方法{@link #prestartCoreThread}或{@link * #prestartAllCoreThreads}动态地覆盖.
 * 如果您使用非空队列构造池，则可能要预启动线程. </dd>
 *
 * <dt>创建新线程</dt>
 *
 * <dd>使用{@link ThreadFactory}创建新线程.  如果没有另行指定，则使用{@link Executors＃defaultThreadFactory},
 * 创建线程的线程全部位于相同的{@link * ThreadGroup}中，并且具有相同的{@code NORM_PRIORITY}优先级和*非守护进程状态.
 * 通过提供其他ThreadFactory，您可以*更改线程的名称，线程组，优先级，守护程序状态,
 * 等等. 如果在询问时{@code ThreadFactory}无法通过从{@code newThread}返回null来创建线程，则执行程序将继续*，但可能无法执行任何任务.
 * 线程*应该拥有“ modifyThread” {@code RuntimePermission}. 如果*使用池的工作线程或其他线程不具有此*权限,
 * 服务可能会降级：配置更改可能不会*及时生效, 并且关闭池可能保持*状态，在该状态下可能终止但未完成.</dd>
 *
 * <dt>保活时间</dt>
 *
 * <dd>如果池当前具有多个corePoolSize线程,
 * 如果多余的线程空闲时间超过keepAliveTime *，则会终止（请参阅{@link #getKeepAliveTime（TimeUnit）}）.
 * 当未积极使用*池时，这提供了减少资源消耗的方法. e
 * 池变得更加活跃*稍后，将构建新线程。 还可以使用方法{@link #setKeepAliveTime（long，* TimeUnit）}动态更改此参数。.
 * 使用{@code Long.MAX_VALUE}的值{@link * TimeUnit＃NANOSECONDS}有效地使空闲线程永远不会*在关闭之前终止.
 * 默认情况下，保持活动策略*仅在corePoolSize线程多时才适用.
 * 但是*方法{@link #allowCoreThreadTimeOut（boolean）}也可以用于*将此超时策略也应用于核心线程，只要keepAliveTime值不为零即可。. </dd>
 *
 * <dt>排队</dt>
 *
 * <dd>任何{@link BlockingQueue}均可用于传输和保留*提交的任务.  此队列的使用与池大小交互：
 *
 * <ul>
 *
 * <li> 如果正在运行的线程少于corePoolSize，则执行器*总是更喜欢添加新线程*而不是排队.</li>
 *
 * <li> 如果corePoolSize或更多线程正在运行，则执行程序*总是更喜欢对请求进行排队，而不是添加新的*线程.</li>
 *
 * <li> 如果无法将请求排队，则除非*超出了maximumPoolSize，否则将创建一个新线程，在这种情况下，该任务将被*拒绝.</li>
 *
 * </ul>
 *
 * 有三种一般的排队策略:
 * <ol>
 *
 * <li> <em> 直接交接.</em> 一个工作队列的不错的默认选择是{@link SynchronousQueue}，它可以将任务交给线程*而不用保留它们.
 * 在这里，如果没有立即可用的线程来运行任务，则尝试将任务*排队失败, 所以将构建一个*新线程.
 * *处理可能具有内部依赖项的请求集时，此策略避免了锁定.
 * 直接切换通常需要无限制的maximumPoolSizes来*避免拒绝新提交的任务.
 * 反过来，这允许*当命令继续平均到达*速度比其可处理速度快时*无限增长线程的可能性.  </li>
 *
 * <li><em> 无限队列.</em> 当所有corePoolSize线程都忙时，使用无界队列（例如*例如，没有预定义*容量的{@link LinkedBlockingQueue}）将导致新任务在队列中等待
 * . 因此，将只创建corePoolSize *线程. (并且maximumPoolSize *的值因此没有任何作用.)
 * 当*每个任务完全独立于其他任务时，这可能是适当的, 因此任务不能*影响彼此的执行；例如,
 * 在网页服务器中。 *虽然这种排队方式对于消除*短暂的请求突发很有用，但它承认*当命令继续以平均速度比其可处理的速度继续到达时，工作队列无限制增长的可能性.  </li>
 *
 * <li><em>有界队列.</em> 当将*与有限的maximumPoolSizes一起使用时，有界队列（例如* {@link ArrayBlockingQueue}）有助于防止资源耗尽,
 * 但可能更难*调整和控制.  队列大小和最大池大小可能会相互折衷*: 使用大队列和小池可以最大程度地减少CPU使用率，操作系统资源和上下文切换开销，但可能导致人为降低吞吐量.  If tasks frequently block (for
 * 例如，如果它们是受I / O约束的，则系统可能能够安排*时间以使线程数量超出您的允许范围. 使用小队列*通常需要更大的池大小，这会使CPU繁忙，但是*可能会遇到不可接受的调度开销，这也会*降低吞吐量.  </li>
 *
 * </ol>
 *
 * </dd>
 *
 * <dt>被拒绝的任务</dt>
 *
 * <dd>当执行器已关闭时，以及在执行器对最大线程数和工作队列使用有限范围时，在方法{@link #execute（Runnable）}中提交的新任务将被<em>拒绝</ em>。容量，并且饱和.
 * 无论哪种情况，{@ code execute} 方法都会调用其 {@link RejectedExecutionHandler}的{@link * RejectedExecutionHandler＃rejectedExecution（Runnable，ThreadPoolExecutor）} *方法。提供了四个预定义的处理程序*策略:
 *
 * <ol>
 *
 * <li> 在默认的{@link ThreadPoolExecutor.AbortPolicy}中，*处理程序在*拒绝时抛出运行时{@link RejectedExecutionException}. </li>
 *
 * <li> 在{@link ThreadPoolExecutor.CallerRunsPolicy}中，调用{@code execute}的线程*本身运行任务。这提供了一个*简单的反馈控制机制，它将降低*新任务提交的速度. </li>
 *
 * <li> 在{@link ThreadPoolExecutor.DiscardPolicy}中，简单地删除了*无法执行的任务.  </li>
 *
 * <li>在{@link ThreadPoolExecutor.DiscardOldestPolicy}中，如果未关闭*执行程序，则将丢弃工作队列*开头的任务，然后重试执行（这可能会再次失败，从而导致该重复。） </li>
 *
 * </ol>
 *
 * 可以定义和使用其他类型的{@link * RejectedExecutionHandler}类。这样做需要一定的注意*尤其是在设计策略仅在特定容量或排队策略下工作时*. </dd>
 *
 * <dt>挂钩方法</dt>
 *
 * <dd>此类提供{@code protected}可重写* {@link #beforeExecute（Thread，Runnable）}和* {@link #afterExecute（Runnable，Throwable）}方法，这些方法在执行每个任务之前和之后被称为*.
 * 这些可用于*操纵执行环境；例如，重新初始化* ThreadLocals，收集统计信息或添加日志条目。
 * 此外，方法{@link #terminated}可以被重写以执行*执行程序*完全终止后需要执行的任何特殊处理.
 *
 * <p>如果钩子或回调方法引发异常，则内部worker *线程可能继而失败并突然终止.</dd>
 *
 * <dt>队列维护</dt>
 *
 * <dd>方法{@link #getQueue（）}允许访问工作队列*以进行监视和调试.
 * 强烈建议不要将此方法用于任何其他目的。两种提供的方法* {@link #remove（Runnable）}和{@link #purge}可用于*取消大量排队任务时*取消存储回收*.</dd>
 *
 * <dt>定案</dt>
 *
 * <dd>程序<em> AND </ em> *中不再引用的池中没有剩余线程，将自动{@code shutdown}.
 * 如果*您甚至希望确保即使在用户忘记调用{@link #shutdown}时也可以回收未引用的池，则必须通过设置适当的* keep-alive时间（使用下限）来设置*未使用的线程
 * 最终死掉零个核心线程和/或*设置{@link #allowCoreThreadTimeOut（boolean）}.  </dd>
 *
 * </dl>
 *
 * <p><b>扩展示例</b>. 此类的大多数扩展*覆盖一个或多个受保护的hook方法。例如，*这是一个子类，它添加了一个简单的暂停/继续功能：
 *
 *  <pre> {@code
 * class PausableThreadPoolExecutor extends ThreadPoolExecutor {
 *   private boolean isPaused;
 *   private ReentrantLock pauseLock = new ReentrantLock();
 *   private Condition unpaused = pauseLock.newCondition();
 *
 *   public PausableThreadPoolExecutor(...) { super(...); }
 *
 *   protected void beforeExecute(Thread t, Runnable r) {
 *     super.beforeExecute(t, r);
 *     pauseLock.lock();
 *     try {
 *       while (isPaused) unpaused.await();
 *     } catch (InterruptedException ie) {
 *       t.interrupt();
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 *
 *   public void pause() {
 *     pauseLock.lock();
 *     try {
 *       isPaused = true;
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 *
 *   public void resume() {
 *     pauseLock.lock();
 *     try {
 *       isPaused = false;
 *       unpaused.signalAll();
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 * }}</pre>
 *
 * @since 1.5
 * @author Doug Lea
 */
public class ThreadPoolExecutor extends AbstractExecutorService {
    /**
     * 主池控制状态ctl是原子整数包装*两个概念字段
     *   workerCount, 指示有效线程数
     *   runState,    指示是否正在运行，正在关闭等
     *
     * 为了将它们打包为一个int，我们将workerCount限制为*（2 ^ 29）-1（约5亿）个线程，而不是（2 ^ 31）-1（2 *十亿），否则可以表示.
     * 如果将来有问题，可以将该变量更改为AtomicLong,
     * 并调整以下移位/遮罩常数. 但是直到需要*出现之前，使用int编写的这段代码会更快，更简单.
     *
     * workerCount是已被*允许启动但未被允许停止的工人数量.  该值可能*与活动线程的实际数量暂时不同,
     * 例如，当ThreadFactory在询问时未能创建线程，并且退出线程仍在终止之前执行簿记操作时。用户可见的池大小报告为*工作集的当前大小.
     *
     * runState提供主要的生命周期控制，采用值:
     *
     *   RUNNING:  接受新任务并处理排队的任务
     *   SHUTDOWN: 不接受新任务，但处理排队的任务
     *   STOP:     不接受新任务，不处理排队的任务,
     *             并中断正在进行的任务
     *   TIDYING:  所有任务已终止，workerCount为零,
     *             转换为状态TIDYING *的线程将运行终止的（）挂钩方法
     *   TERMINATED: 已完成
     *
     * 这些值之间的数字顺序很重要, 允许*有序比较.
     * runState随着*时间单调增加, 但不必打到每个状态。过渡是:
     *
     * RUNNING -> SHUTDOWN
     *    On invocation of shutdown(), perhaps implicitly in finalize()
     * (RUNNING or SHUTDOWN) -> STOP
     *    On invocation of shutdownNow()
     * SHUTDOWN -> TIDYING
     *    When both queue and pool are empty
     * STOP -> TIDYING
     *    When pool is empty
     * TIDYING -> TERMINATED
     *    When the terminated() hook method has completed
     *
     * 当*状态达到TERMINATED时，在awaitTermination（）中等待的线程将返回.
     *
     * *检测从SHUTDOWN到TIDYING的过渡比您想要的要简单得多，因为在SHUTDOWN状态期间，队列在非空之后可能变为空，反之亦然,
     * but
     * we can only terminate if, after seeing that it is empty, we see
     * that workerCount is 0 (which sometimes entails a recheck -- see
     * below).
     */

    // 控制变量-存放状态和线程数
    private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));
    private static final int COUNT_BITS = Integer.SIZE - 3;
    private static final int CAPACITY   = (1 << COUNT_BITS) - 1;

    // runState存储在高位
    private static final int RUNNING    = -1 << COUNT_BITS;
    private static final int SHUTDOWN   =  0 << COUNT_BITS;
    private static final int STOP       =  1 << COUNT_BITS;
    private static final int TIDYING    =  2 << COUNT_BITS;
    private static final int TERMINATED =  3 << COUNT_BITS;

    // Packing and unpacking ctl
    private static int runStateOf(int c)     { return c & ~CAPACITY; }
    private static int workerCountOf(int c)  { return c & CAPACITY; }
    private static int ctlOf(int rs, int wc) { return rs | wc; }

    /*
     * Bit field accessors that don't require unpacking ctl.
     * These depend on the bit layout and on workerCount being never negative.
     */

    private static boolean runStateLessThan(int c, int s) {
        return c < s;
    }

    private static boolean runStateAtLeast(int c, int s) {
        return c >= s;
    }

    private static boolean isRunning(int c) {
        return c < SHUTDOWN;
    }

    /**
     * Attempts to CAS-increment the workerCount field of ctl.
     */
    private boolean compareAndIncrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect + 1);
    }

    /**
     * Attempts to CAS-decrement the workerCount field of ctl.
     */
    private boolean compareAndDecrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect - 1);
    }

    /**
     * Decrements the workerCount field of ctl. This is called only on
     * abrupt termination of a thread (see processWorkerExit). Other
     * decrements are performed within getTask.
     */
    private void decrementWorkerCount() {
        do {} while (! compareAndDecrementWorkerCount(ctl.get()));
    }

    /**
     * 用于保留任务并移交给工作线程的队列.  我们不要求workQueue.poll（）返回* null必然意味着workQueue.isEmpty（）,
     * 因此，仅依靠isEmpty来查看队列是否为空（例如，在决定是否从* SHUTDOWN过渡到TIDYING时，我们必须*这样做）.
     * 这可容纳特殊的*队列，例如DelayQueues，允许poll（）返回null，即使延迟可能在延迟*到期后返回非null时也是如此.
     */
    private final BlockingQueue<Runnable> workQueue;

    /**
     * 锁定时要锁定工人位置和相关簿记。 *虽然我们可以使用某种并发集，但事实证明*通常最好使用锁.
     * 原因之一是*序列化了interruptIdleWorkers，从而避免了*不必要的中断风暴，尤其是在关机期间.
     * 否则退出线程会同时中断那些尚未中断的*.
     * 它还简化了一些与* PastSize等相关的统计簿记. 我们*在关闭和shutdownNow时也保持mainLock，以便*确保工人集稳定，同时分别检查*允许中断和实际中断的权限.
     */
    private final ReentrantLock mainLock = new ReentrantLock();

    /**
     * 集包含池中的所有工作线程。仅在*保持mainLock时访问.
     */
    private final HashSet<Worker> workers = new HashSet<Worker>();

    /**
     * 等待条件以支持awaitTermination
     */
    private final Condition termination = mainLock.newCondition();

    /**
     * 跟踪达到的最大池大小。仅在* mainLock下访问.
     */
    private int largestPoolSize;

    /**
     * 计数器完成的任务。仅在终止*工作线程时更新。仅在mainLock下访问.
     */
    private long completedTaskCount;

    /*
     * 所有用户控制参数都声明为volatile，以便*正在进行的操作基于最新的值，但不需要*进行锁定，
     * 因为没有内部不变量依赖于它们*与其他操作同步更改.
     */

    /**
     * 新线程的工厂。所有线程都是使用此*工厂创建的（通过addWorker方法）.
     * 必须为所有调用程序做好准备，以使addWorker失败，这可能反映出系统或用户的策略限制了线程数.
     * 即使未将其视为错误，创建线程失败也可能导致新任务被拒绝或现有任务仍停留在队列中.
     *
     * 我们走得更远，即使遇到*错误（例如OutOfMemoryError），它们也会保留池不变式，而*在尝试创建线程时可能会抛出.
     * 由于*需要在Thread.start中分配本机堆栈，因此此类错误相当常见，并且用户*将要执行清理池关闭以进行清理.
     * *可能有足够的内存来供清理代码完成*而不遇到另一个OutOfMemoryError.
     */
    private volatile ThreadFactory threadFactory;

    /**
     * 执行饱和或关闭时调用处理程序.
     */
    private volatile RejectedExecutionHandler handler;

    /**
     * 空闲线程等待工作的超时时间（以纳秒为单位）.
     * 当存在更多的corePoolSize *或allowCoreThreadTimeOut时，线程使用此超时.
     * 否则，他们将永远等着新工作.
     */
    private volatile long keepAliveTime;

    /**
     * 如果为false（默认），则即使处于空闲状态，核心线程也保持活动状态.
     * 如果为true，则核心线程使用keepAliveTime来超时等待*工作.
     */
    private volatile boolean allowCoreThreadTimeOut;

    /**
     * 除非设置了allowCoreThreadTimeOut *，否则核心池大小是保持活动状态*（并且不允许超时等）的最低数量，在这种情况下，最小值为零.
     */
    private volatile int corePoolSize;

    /**
     * 最大池大小。请注意，实际最大值在内部*受CAPACITY限制.
     */
    private volatile int maximumPoolSize;

    /**
     * 默认拒绝执行处理程序
     */
    private static final RejectedExecutionHandler defaultHandler =
        new AbortPolicy();

    /**
     * 关机和关机的调用者需要权限.
     * 我们还要求（请参阅checkShutdownAccess），调用方*有权实际中断工作程序集中的线程*
     * （由Thread.interrupt所控制，后者依赖于* ThreadGroup.checkAccess，
     * 而ThreadGroup.checkAccess则依赖于* SecurityManager.checkAccess）。仅当*这些检查通过时才尝试关机.
     *
     * 所有实际的线程调用.中断（请参阅* interruptIdleWorkers和interruptWorkers）
     * 忽略* SecurityExceptions，这意味着尝试的中断*静默失败.
     * 在关机的情况下，它们不会失败*除非SecurityManager的策略不一致，
     * 有时*允许访问线程，有时不允许访问。
     * 在这种情况下，无法真正中断线程可能会禁用或延迟完全终止。
     * 可以使用interruptIdleWorkers的其他用途，*并且*实际中断的失败只会延迟*配置更改的响应，因此不会进行特殊处理.
     */
    private static final RuntimePermission shutdownPerm =
        new RuntimePermission("modifyThread");

    /* 执行终结器时要使用的上下文，或者为null. */
    private final AccessControlContext acc;

    /**
     * Class Worker主要维护*线程运行任务的中断控制状态，以及其他次要簿记.
     * 此类机会性地扩展AbstractQueuedSynchronizer *以简化获取和释放围绕每个*任务执行的锁.
     * 这可以防止旨在*唤醒等待任务的工作线程从*而不是中断正在运行的任务的中断.
     * 我们实现一个简单的*非可重入互斥锁，而不是使用* ReentrantLock，因为我们不希望工作人员任务在调用诸如setCorePoolSize
     * 之类的池控制方法时能够重新获取该锁。
     * 另外，为了在线程真正开始运行任务之前抑制中断，我们将lock状态初始化为负值，并在启动时将其清除（在runWorker中）.
     */
    private final class Worker
        extends AbstractQueuedSynchronizer
        implements Runnable
    {
        /**
         * 此类永远不会序列化，但是我们提供了 serialVersionUID来禁止Javac警告.
         */
        private static final long serialVersionUID = 6138294804551838833L;

        /** 该工人正在运行的线程。如果工厂失败，则为null. */
        final Thread thread;
        /** 要运行的初始任务。可能为空. */
        Runnable firstTask;
        /** 每线程任务计数器 */
        volatile long completedTasks;

        /**
         * 使用给定的第一个任务和ThreadFactory中的线程创建。 * @param firstTask第一个任务 (null if none)
         */
        Worker(Runnable firstTask) {
            setState(-1); // 禁止中断，直到runWorker
            this.firstTask = firstTask;
            this.thread = getThreadFactory().newThread(this);
        }

        /** 将主运行循环委托给外部runWorker  */
        public void run() {
            runWorker(this);
        }

        // Lock methods
        //
        // The value 0 represents the unlocked state.
        // The value 1 represents the locked state.
        //  是否持有独占锁，state值为1的时候表示持有锁，state值为0的时候表示已经释放锁
        protected boolean isHeldExclusively() {
            return getState() != 0;
        }

        // 独占模式下尝试获取资源，这里没有判断传入的变量，直接CAS判断0更新为1是否成功，成功则设置独占线程为当前线程
        protected boolean tryAcquire(int unused) {
            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        protected boolean tryRelease(int unused) {
            setExclusiveOwnerThread(null);
            setState(0);
            return true;
        }

        public void lock()        { acquire(1); }
        public boolean tryLock()  { return tryAcquire(1); }
        public void unlock()      { release(1); }
        public boolean isLocked() { return isHeldExclusively(); }
        // 启动后进行线程中断，注意这里会判断线程实例的中断标志位是否为false，只有中断标志位为false才会中断
        void interruptIfStarted() {
            Thread t;
            if (getState() >= 0 && (t = thread) != null && !t.isInterrupted()) {
                try {
                    t.interrupt();
                } catch (SecurityException ignore) {
                }
            }
        }
    }

    /*
     * Methods for setting control state
     */

    /**
     * Transitions runState to given target, or leaves it alone if
     * already at least the given target.
     *
     * @param targetState the desired state, either SHUTDOWN or STOP
     *        (but not TIDYING or TERMINATED -- use tryTerminate for that)
     */
    private void advanceRunState(int targetState) {
        for (;;) {
            int c = ctl.get();
            if (runStateAtLeast(c, targetState) ||
                ctl.compareAndSet(c, ctlOf(targetState, workerCountOf(c))))
                break;
        }
    }

    /**
     * Transitions to TERMINATED state if either (SHUTDOWN and pool
     * and queue empty) or (STOP and pool empty).  If otherwise
     * eligible to terminate but workerCount is nonzero, interrupts an
     * idle worker to ensure that shutdown signals propagate. This
     * method must be called following any action that might make
     * termination possible -- reducing worker count or removing tasks
     * from the queue during shutdown. The method is non-private to
     * allow access from ScheduledThreadPoolExecutor.
     */
    final void tryTerminate() {
        for (;;) {
            int c = ctl.get();
            if (isRunning(c) ||
                runStateAtLeast(c, TIDYING) ||
                (runStateOf(c) == SHUTDOWN && ! workQueue.isEmpty()))
                return;
            if (workerCountOf(c) != 0) { // Eligible to terminate
                interruptIdleWorkers(ONLY_ONE);
                return;
            }

            final ReentrantLock mainLock = this.mainLock;
            mainLock.lock();
            try {
                if (ctl.compareAndSet(c, ctlOf(TIDYING, 0))) {
                    try {
                        terminated();
                    } finally {
                        ctl.set(ctlOf(TERMINATED, 0));
                        termination.signalAll();
                    }
                    return;
                }
            } finally {
                mainLock.unlock();
            }
            // else retry on failed CAS
        }
    }

    /*
     * Methods for controlling interrupts to worker threads.
     */

    /**
     * If there is a security manager, makes sure caller has
     * permission to shut down threads in general (see shutdownPerm).
     * If this passes, additionally makes sure the caller is allowed
     * to interrupt each worker thread. This might not be true even if
     * first check passed, if the SecurityManager treats some threads
     * specially.
     */
    private void checkShutdownAccess() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(shutdownPerm);
            final ReentrantLock mainLock = this.mainLock;
            mainLock.lock();
            try {
                for (Worker w : workers)
                    security.checkAccess(w.thread);
            } finally {
                mainLock.unlock();
            }
        }
    }

    /**
     * Interrupts all threads, even if active. Ignores SecurityExceptions
     * (in which case some threads may remain uninterrupted).
     */
    private void interruptWorkers() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            for (Worker w : workers)
                w.interruptIfStarted();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Interrupts threads that might be waiting for tasks (as
     * indicated by not being locked) so they can check for
     * termination or configuration changes. Ignores
     * SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     *
     * @param onlyOne If true, interrupt at most one worker. This is
     * called only from tryTerminate when termination is otherwise
     * enabled but there are still other workers.  In this case, at
     * most one waiting worker is interrupted to propagate shutdown
     * signals in case all threads are currently waiting.
     * Interrupting any arbitrary thread ensures that newly arriving
     * workers since shutdown began will also eventually exit.
     * To guarantee eventual termination, it suffices to always
     * interrupt only one idle worker, but shutdown() interrupts all
     * idle workers so that redundant workers exit promptly, not
     * waiting for a straggler task to finish.
     */
    private void interruptIdleWorkers(boolean onlyOne) {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            for (Worker w : workers) {
                Thread t = w.thread;
                if (!t.isInterrupted() && w.tryLock()) {
                    try {
                        t.interrupt();
                    } catch (SecurityException ignore) {
                    } finally {
                        w.unlock();
                    }
                }
                if (onlyOne)
                    break;
            }
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Common form of interruptIdleWorkers, to avoid having to
     * remember what the boolean argument means.
     */
    private void interruptIdleWorkers() {
        interruptIdleWorkers(false);
    }

    private static final boolean ONLY_ONE = true;

    /*
     * Misc utilities, most of which are also exported to
     * ScheduledThreadPoolExecutor
     */

    /**
     * 调用给定命令的拒绝执行处理程序.
     * Package-protected for use by ScheduledThreadPoolExecutor.
     */
    final void reject(Runnable command) {
        handler.rejectedExecution(command, this);
    }

    /**
     * Performs any further cleanup following run state transition on
     * invocation of shutdown.  A no-op here, but used by
     * ScheduledThreadPoolExecutor to cancel delayed tasks.
     */
    void onShutdown() {
    }

    /**
     * State check needed by ScheduledThreadPoolExecutor to
     * enable running tasks during shutdown.
     *
     * @param shutdownOK true if should return true if SHUTDOWN
     */
    final boolean isRunningOrShutdown(boolean shutdownOK) {
        int rs = runStateOf(ctl.get());
        return rs == RUNNING || (rs == SHUTDOWN && shutdownOK);
    }

    /**
     * Drains the task queue into a new list, normally using
     * drainTo. But if the queue is a DelayQueue or any other kind of
     * queue for which poll or drainTo may fail to remove some
     * elements, it deletes them one by one.
     */
    private List<Runnable> drainQueue() {
        BlockingQueue<Runnable> q = workQueue;
        ArrayList<Runnable> taskList = new ArrayList<Runnable>();
        q.drainTo(taskList);
        if (!q.isEmpty()) {
            for (Runnable r : q.toArray(new Runnable[0])) {
                if (q.remove(r))
                    taskList.add(r);
            }
        }
        return taskList;
    }

    /*
     * 工作人员创建，运行和清理的方法
     */

    /**
     * 检查是否可以根据当前*池状态和给定的边界（核心或最大值）添加新的工作程序.
     * 如果是这样，将调整工作人员计数，并在可能的情况下创建并启动一个新的工作人员，并将firstTask作为其第一个任务运行.
     * 如果池已停止或*可以关闭，则此方法返回false。
     * 如果在询问时线程*工厂无法创建线程，它也会返回false。
     * 如果线程创建失败（由于线程工厂返回* null或由于异常）（通常是* Thread.start（）中的OutOfMemoryError），我们会干净地回滚.
     *
     * @param firstTask新线程应首先运行的任务（如果没有，则为* null）。当初始线程的数量少于corePoolSize线程（在这种情况下，我们总是启动一个），
     *                            *或队列已满时（在这种情况下，我们必须使用初始的第一个任务*（在execute（）方法中）创建工作程序，以绕过队列绕过队列）。
     *                            *最初，空闲线程通常是通过* prestartCoreThread创建的，或用于替换其他垂死的工人.
     *
     * @param core如果为true，请使用corePoolSize作为绑定，否则* maximumPoolSize。 （此处使用布尔值指示符，而不是*值，以确保在检查其他pool *状态后读取新值）.
     * @return true if successful
     * // 添加工作线程，如果返回false说明没有新创建工作线程，如果返回true说明创建和启动工作线程成功
     */
    private boolean addWorker(Runnable firstTask, boolean core) {
        retry:
        // 注意这是一个死循环 - 最外层循环
        //for (int c = ctl.get();;)
        for (;;) {
            int c = ctl.get();
            int rs = runStateOf(c);
            // 这个是十分复杂的条件，这里先拆分多个与（&&）条件：
            // 1. 线程池状态至少为SHUTDOWN状态，也就是rs >= SHUTDOWN(0)
            // 2. 线程池状态至少为STOP状态，也就是rs >= STOP(1)，或者传入的任务实例firstTask不为null，或者任务队列为空
            // 其实这个判断的边界是线程池状态为shutdown状态下，不会再接受新的任务，在此前提下如果状态已经到了STOP、或者传入任务不为空、
            // 或者任务队列为空（已经没有积压任务）都不需要添加新的线程
            // Check if queue empty only if necessary.
            if (rs >= SHUTDOWN &&
                ! (rs == SHUTDOWN &&
                   firstTask == null &&
                   ! workQueue.isEmpty()))
                return false;
            // 这里每一轮循环都会重新获取工作线程数wc
            // 1. 如果传入的core为true，表示将要创建核心线程，通过wc和corePoolSize判断，如果wc >= corePoolSize，则返回false表示创建核心线程失败
            // 1. 如果传入的core为false，表示将要创非建核心线程，通过wc和maximumPoolSize判断，如果wc >= maximumPoolSize，则返回false表示创建非核心线程失败
            for (;;) {
                int wc = workerCountOf(c);
                if (wc >= CAPACITY ||
                    wc >= (core ? corePoolSize : maximumPoolSize))
                    return false;
                // 成功通过CAS更新工作线程数wc，则break到最外层的循环
                if (compareAndIncrementWorkerCount(c))
                    break retry;
                // 走到这里说明了通过CAS更新工作线程数wc失败，这个时候需要重新判断线程池的状态是否由RUNNING已经变为SHUTDOWN
                c = ctl.get();  // Re-read ctl
                // 如果线程池状态已经由RUNNING已经变为SHUTDOWN，则重新跳出到外层循环继续执行
                if (runStateOf(c) != rs)
                    // 如果线程池状态依然是RUNNING，CAS更新工作线程数wc失败说明有可能是并发更新导致的失败，则在内层循环重试即可
                    continue retry;
                // else CAS failed due to workerCount change; retry inner loop
            }
        }
        // 标记工作线程是否启动成功
        boolean workerStarted = false;
        // 标记工作线程是否创建成功
        boolean workerAdded = false;
        Worker w = null;
        try {
            // 传入任务实例firstTask创建Worker实例，Worker构造里面会通过线程工厂创建新的Thread对象，所以下面可以直接操作Thread t = w.thread
            // 这一步Worker实例已经创建，但是没有加入工作线程集合或者启动它持有的线程Thread实例
            w = new Worker(firstTask);
            final Thread t = w.thread;
            if (t != null) {
                // 这里需要全局加锁，因为会改变一些指标值和非线程安全的集合
                final ReentrantLock mainLock = this.mainLock;
                mainLock.lock();
                try {
                    // 按住锁时重新检查.
                    // 如果ThreadFactory失败或//在获得锁之前关闭，请退回.
                    int rs = runStateOf(ctl.get());
                    // 这里主要在加锁的前提下判断ThreadFactory创建的线程是否存活或者判断获取锁成功之后线程池状态是否已经更变为SHUTDOWN
                    // 1. 如果线程池状态依然为RUNNING，则只需要判断线程实例是否存活，需要添加到工作线程集合和启动新的Worker
                    // 2. 如果线程池状态小于STOP，也就是RUNNING或者SHUTDOWN状态下，同时传入的任务实例firstTask为null，则需要添加到工作线程集合和启动新的Worker
                    // 对于2，换言之，如果线程池处于SHUTDOWN状态下，同时传入的任务实例firstTask不为null，则不会添加到工作线程集合和启动新的Worker
                    // 这一步其实有可能创建了新的Worker实例但是并不启动（临时对象，没有任何强引用），这种Worker有可能成功下一轮GC被收集的垃圾对象
                    if (rs < SHUTDOWN ||
                        (rs == SHUTDOWN && firstTask == null)) {
                        if (t.isAlive()) // precheck that t is startable
                            throw new IllegalThreadStateException();
                        // 把创建的工作线程实例添加到工作线程集合
                        workers.add(w);
                        int s = workers.size();
                        // 尝试更新历史峰值工作线程数，也就是线程池峰值容量
                        if (s > largestPoolSize)
                            largestPoolSize = s;
                        // 这里更新工作线程是否启动成功标识为true，后面才会调用Thread#start()方法启动真实的线程实例
                        workerAdded = true;
                    }
                } finally {
                    mainLock.unlock();
                }
                // 如果成功添加工作线程，则调用Worker内部的线程实例t的Thread#start()方法启动真实的线程实例
                if (workerAdded) {
                    t.start();
                    workerStarted = true;
                }
            }
        } finally {
            // 线程启动失败，需要从工作线程集合移除对应的Worker
            if (! workerStarted)
                addWorkerFailed(w);
        }
        return workerStarted;
    }

    /**
     * 回滚工作线程创建.
     * - 从工人中删除工人（如果存在）*-减少工人数量*-重新检查是否终止, 如果这个*工人的存在阻止了解雇
     */
    private void addWorkerFailed(Worker w) {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 从工作线程集合移除之
            if (w != null)
                workers.remove(w);
            // wc数量减1
            decrementWorkerCount();
            // 基于状态判断尝试终结线程池
            tryTerminate();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Performs cleanup and bookkeeping for a dying worker. Called
     * only from worker threads. Unless completedAbruptly is set,
     * assumes that workerCount has already been adjusted to account
     * for exit.  This method removes thread from worker set, and
     * possibly terminates the pool or replaces the worker if either
     * it exited due to user task exception or if fewer than
     * corePoolSize workers are running or queue is non-empty but
     * there are no workers.
     *
     * @param w the worker
     * @param completedAbruptly if the worker died due to user exception
     */
    private void processWorkerExit(Worker w, boolean completedAbruptly) {
        if (completedAbruptly) // If abrupt, then workerCount wasn't adjusted
            decrementWorkerCount();

        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            completedTaskCount += w.completedTasks;
            workers.remove(w);
        } finally {
            mainLock.unlock();
        }

        tryTerminate();

        int c = ctl.get();
        if (runStateLessThan(c, STOP)) {
            if (!completedAbruptly) {
                int min = allowCoreThreadTimeOut ? 0 : corePoolSize;
                if (min == 0 && ! workQueue.isEmpty())
                    min = 1;
                if (workerCountOf(c) >= min)
                    return; // replacement not needed
            }
            addWorker(null, false);
        }
    }

    /**
     * 根据当前配置设置执行阻止或定时等待任务, 或如果此工作人员*由于以下原因必须退出，则返回null：:
     * 1. There are more than maximumPoolSize workers (due to
     *    a call to setMaximumPoolSize).
     * 2. The pool is stopped.
     * 3. The pool is shutdown and the queue is empty.
     * 4. This worker timed out waiting for a task, and timed-out
     *    workers are subject to termination (that is,
     *    {@code allowCoreThreadTimeOut || workerCount > corePoolSize})
     *    both before and after the timed wait, and if the queue is
     *    non-empty, this worker is not the last thread in the pool.
     *
     * @return task, or null if the worker must exit, in which case
     *         workerCount is decremented
     */
    private Runnable getTask() {
        boolean timedOut = false; // Did the last poll() time out?

        for (;;) {
            int c = ctl.get();
            int rs = runStateOf(c);

            // Check if queue empty only if necessary.
            if (rs >= SHUTDOWN && (rs >= STOP || workQueue.isEmpty())) {
                decrementWorkerCount();
                return null;
            }

            int wc = workerCountOf(c);

            // Are workers subject to culling?
            boolean timed = allowCoreThreadTimeOut || wc > corePoolSize;

            if ((wc > maximumPoolSize || (timed && timedOut))
                && (wc > 1 || workQueue.isEmpty())) {
                if (compareAndDecrementWorkerCount(c))
                    return null;
                continue;
            }

            try {
                Runnable r = timed ?
                    workQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS) :
                    workQueue.take();
                if (r != null)
                    return r;
                timedOut = true;
            } catch (InterruptedException retry) {
                timedOut = false;
            }
        }
    }

    /**
     * 主工人运行循环.  反复从队列中获取任务并*执行它们，同时解决许多问题:
     *
     * 1. 我们可能从一个初始任务开始，在这种情况下，我们不需要获得第一个任务。
     * 否则，只要pool正在运行，我们就会从getTask获得任务。如果返回null，则*工作程序由于池状态或配置参数更改而退出。
     * 其他退出是由*外部代码中的异常引发而导致的，在这种情况下，completedAbruptly成立，这通常导致processWorkerExit替换此线程.
     *
     * 2. Before running any task, the lock is acquired to prevent
     * other pool interrupts while the task is executing, and then we
     * ensure that unless pool is stopping, this thread does not have
     * its interrupt set.
     *
     * 3. Each task run is preceded by a call to beforeExecute, which
     * might throw an exception, in which case we cause thread to die
     * (breaking loop with completedAbruptly true) without processing
     * the task.
     *
     * 4. Assuming beforeExecute completes normally, we run the task,
     * gathering any of its thrown exceptions to send to afterExecute.
     * We separately handle RuntimeException, Error (both of which the
     * specs guarantee that we trap) and arbitrary Throwables.
     * Because we cannot rethrow Throwables within Runnable.run, we
     * wrap them within Errors on the way out (to the thread's
     * UncaughtExceptionHandler).  Any thrown exception also
     * conservatively causes thread to die.
     *
     * 5. After task.run completes, we call afterExecute, which may
     * also throw an exception, which will also cause thread to
     * die. According to JLS Sec 14.20, this exception is the one that
     * will be in effect even if task.run throws.
     *
     * The net effect of the exception mechanics is that afterExecute
     * and the thread's UncaughtExceptionHandler have as accurate
     * information as we can provide about any problems encountered by
     * user code.
     *
     * @param w the worker
     */
    final void runWorker(Worker w) {
        // 获取当前线程，实际上和Worker持有的线程实例是相同的
        Thread wt = Thread.currentThread();
        // 获取Worker中持有的初始化时传入的任务对象，这里注意存放在临时变量task中
        Runnable task = w.firstTask;
        // 设置Worker中持有的初始化时传入的任务对象为null
        w.firstTask = null;
        // 由于Worker初始化时AQS中state设置为-1，这里要先做一次解锁把state更新为0，允许线程中断
        w.unlock(); // allow interrupts
        // 记录线程是否因为用户异常终结，默认是true
        boolean completedAbruptly = true;
        try {
            while (task != null || (task = getTask()) != null) {
                w.lock();
                // If pool is stopping, ensure thread is interrupted;
                // if not, ensure thread is not interrupted.  This
                // requires a recheck in second case to deal with
                // shutdownNow race while clearing interrupt
                if ((runStateAtLeast(ctl.get(), STOP) ||
                     (Thread.interrupted() &&
                      runStateAtLeast(ctl.get(), STOP))) &&
                    !wt.isInterrupted())
                    wt.interrupt();
                try {
                    beforeExecute(wt, task);
                    Throwable thrown = null;
                    try {
                        task.run();
                    } catch (RuntimeException x) {
                        thrown = x; throw x;
                    } catch (Error x) {
                        thrown = x; throw x;
                    } catch (Throwable x) {
                        thrown = x; throw new Error(x);
                    } finally {
                        afterExecute(task, thrown);
                    }
                } finally {
                    task = null;
                    w.completedTasks++;
                    w.unlock();
                }
            }
            completedAbruptly = false;
        } finally {
            processWorkerExit(w, completedAbruptly);
        }
    }

    // 公共构造函数和方法

    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             Executors.defaultThreadFactory(), defaultHandler);
    }

    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             threadFactory, defaultHandler);
    }

    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              RejectedExecutionHandler handler) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             Executors.defaultThreadFactory(), handler);
    }

    /**
     * Creates a new {@code ThreadPoolExecutor} with the given initial
     * parameters.
     *
     * @param corePoolSize 保留在池中的线​​程数，即使处于空闲状态，除非设置了{@code allowCoreThreadTimeOut}
     * @param maximumPoolSize 池中允许的最大线程数
     * @param 当线程数大于*核心数时的keepAliveTime，这是多余的空闲线程*在终止之前等待新任务的最长时间.
     * @param unit the time unit for the {@code keepAliveTime} argument
     * @param workQueue 在执行任务之前用于保留任务的队列。此队列将仅保存{@code execute}方法提交的{@code Runnable} *任务.
     * @param threadFactory 执行程序*创建新线程时要使用的工厂
     * @param handler 当执行被阻塞时要使用的处理程序*因为达到了线程界限和队列容量
     * @throws IllegalArgumentException if one of the following holds:<br>
     *         {@code corePoolSize < 0}<br>
     *         {@code keepAliveTime < 0}<br>
     *         {@code maximumPoolSize <= 0}<br>
     *         {@code maximumPoolSize < corePoolSize}
     * @throws NullPointerException if {@code workQueue}
     *         or {@code threadFactory} or {@code handler} is null
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }

    /**
     * 在将来的某个时间执行给定的任务.  任务可以在新线程或现有池线程中执行.
     *
     * 如果任务无法提交执行, 是因为此执行程序已关闭，还是因为其容量已达到,
     * 任务由当前处理{@code RejectedExecutionHandler}.
     *
     * @param command the task to execute
     */
    public void execute(Runnable command) {
        if (command == null)
            throw new NullPointerException();
        /*
         * 进行3个步骤:
         *
         * 1. 如果少于正在运行的corePoolSize线程, 尝试以给定的命令作为第一个任务来启动新线程.
         * 对addWorker的调用原子检查runState和 workerCount, 并因此通过返回false来防止在不应该添加线程的情况下产生虚假警报.
         *
         * 2. 如果任务可以成功排队, 那么我们仍然需要仔细检查我们是否应该添加一个线程（因为现有线程自上次检查后就死亡了）或自进入此方法以来该池已关闭.
         * 因此，我们重新检查状态，并在必要时回滚排队，如果*已停止，或者如果没有线程则启动一个新线程.
         *
         * 3. 如果我们不能排队任务, 然后我们尝试添加一个新的线程。如果失败, 我们知道我们已经关闭或处于饱和状态因此拒绝了任务.
         */
        int c = ctl.get();
        // 判断如果当前工作线程数小于核心线程数，则创建新的核心线程并且执行传入的任务
        if (workerCountOf(c) < corePoolSize) {
            if (addWorker(command, true))
                return;
            c = ctl.get();
        }
        // 走到这里说明创建新的核心线程失败，也就是当前工作线程数大于等于corePoolSize
        // 判断线程池是否处于运行中状态，同时尝试用非阻塞方法向任务队列放入任务（放入任务失败返回false）
        if (isRunning(c) && workQueue.offer(command)) {
            int recheck = ctl.get();
            // 这里是向任务队列投放任务成功，对线程池的运行中状态做二次检查
            // 如果线程池二次检查状态是非运行中状态，则从任务队列移除当前的任务调用拒绝策略处理之（也就是移除前面成功入队的任务实例）
            if (! isRunning(recheck) && remove(command))
                reject(command);
                // 走到下面的else if分支，说明有以下的前提：
                // 0、待执行的任务已经成功加入任务队列
                // 1、线程池可能是RUNNING状态
                // 2、传入的任务可能从任务队列中移除失败（移除失败的唯一可能就是任务已经被执行了）
                // 如果当前工作线程数量为0，则创建一个非核心线程并且传入的任务对象为null - 返回
                // 也就是创建的非核心线程不会马上运行，而是等待获取任务队列的任务去执行
                // 如果前工作线程数量不为0，原来应该是最后的else分支，但是可以什么也不做，因为任务已经成功入队列，总会有合适的时机分配其他空闲线程去执行它
            else if (workerCountOf(recheck) == 0)
                addWorker(null, false);
        }
        // 走到这里说明有以下的前提：
        // 0、线程池中的工作线程总数已经大于等于corePoolSize（简单来说就是核心线程已经全部懒创建完毕）
        // 1、线程池可能不是RUNNING状态
        // 2、线程池可能是RUNNING状态同时任务队列已经满了
        // 如果向任务队列投放任务失败，则会尝试创建非核心线程传入任务执行
        // 创建非核心线程失败，此时需要拒绝执行任务
        else if (!addWorker(command, false))
            reject(command);
    }

    /**
     * Initiates an orderly shutdown in which previously submitted
     * tasks are executed, but no new tasks will be accepted.
     * Invocation has no additional effect if already shut down.
     *
     * <p>This method does not wait for previously submitted tasks to
     * complete execution.  Use {@link #awaitTermination awaitTermination}
     * to do that.
     *
     * @throws SecurityException {@inheritDoc}
     */
    public void shutdown() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            checkShutdownAccess();
            advanceRunState(SHUTDOWN);
            interruptIdleWorkers();
            onShutdown(); // hook for ScheduledThreadPoolExecutor
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
    }

    /**
     * Attempts to stop all actively executing tasks, halts the
     * processing of waiting tasks, and returns a list of the tasks
     * that were awaiting execution. These tasks are drained (removed)
     * from the task queue upon return from this method.
     *
     * <p>This method does not wait for actively executing tasks to
     * terminate.  Use {@link #awaitTermination awaitTermination} to
     * do that.
     *
     * <p>There are no guarantees beyond best-effort attempts to stop
     * processing actively executing tasks.  This implementation
     * cancels tasks via {@link Thread#interrupt}, so any task that
     * fails to respond to interrupts may never terminate.
     *
     * @throws SecurityException {@inheritDoc}
     */
    public List<Runnable> shutdownNow() {
        List<Runnable> tasks;
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            checkShutdownAccess();
            advanceRunState(STOP);
            interruptWorkers();
            tasks = drainQueue();
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
        return tasks;
    }

    public boolean isShutdown() {
        return ! isRunning(ctl.get());
    }

    /**
     * Returns true if this executor is in the process of terminating
     * after {@link #shutdown} or {@link #shutdownNow} but has not
     * completely terminated.  This method may be useful for
     * debugging. A return of {@code true} reported a sufficient
     * period after shutdown may indicate that submitted tasks have
     * ignored or suppressed interruption, causing this executor not
     * to properly terminate.
     *
     * @return {@code true} if terminating but not yet terminated
     */
    public boolean isTerminating() {
        int c = ctl.get();
        return ! isRunning(c) && runStateLessThan(c, TERMINATED);
    }

    public boolean isTerminated() {
        return runStateAtLeast(ctl.get(), TERMINATED);
    }

    public boolean awaitTermination(long timeout, TimeUnit unit)
        throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            for (;;) {
                if (runStateAtLeast(ctl.get(), TERMINATED))
                    return true;
                if (nanos <= 0)
                    return false;
                nanos = termination.awaitNanos(nanos);
            }
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Invokes {@code shutdown} when this executor is no longer
     * referenced and it has no threads.
     */
    protected void finalize() {
        SecurityManager sm = System.getSecurityManager();
        if (sm == null || acc == null) {
            shutdown();
        } else {
            PrivilegedAction<Void> pa = () -> { shutdown(); return null; };
            AccessController.doPrivileged(pa, acc);
        }
    }

    /**
     * Sets the thread factory used to create new threads.
     *
     * @param threadFactory the new thread factory
     * @throws NullPointerException if threadFactory is null
     * @see #getThreadFactory
     */
    public void setThreadFactory(ThreadFactory threadFactory) {
        if (threadFactory == null)
            throw new NullPointerException();
        this.threadFactory = threadFactory;
    }

    /**
     * Returns the thread factory used to create new threads.
     *
     * @return the current thread factory
     * @see #setThreadFactory(ThreadFactory)
     */
    public ThreadFactory getThreadFactory() {
        return threadFactory;
    }

    /**
     * Sets a new handler for unexecutable tasks.
     *
     * @param handler the new handler
     * @throws NullPointerException if handler is null
     * @see #getRejectedExecutionHandler
     */
    public void setRejectedExecutionHandler(RejectedExecutionHandler handler) {
        if (handler == null)
            throw new NullPointerException();
        this.handler = handler;
    }

    /**
     * Returns the current handler for unexecutable tasks.
     *
     * @return the current handler
     * @see #setRejectedExecutionHandler(RejectedExecutionHandler)
     */
    public RejectedExecutionHandler getRejectedExecutionHandler() {
        return handler;
    }

    /**
     * Sets the core number of threads.  This overrides any value set
     * in the constructor.  If the new value is smaller than the
     * current value, excess existing threads will be terminated when
     * they next become idle.  If larger, new threads will, if needed,
     * be started to execute any queued tasks.
     *
     * @param corePoolSize the new core size
     * @throws IllegalArgumentException if {@code corePoolSize < 0}
     * @see #getCorePoolSize
     */
    public void setCorePoolSize(int corePoolSize) {
        if (corePoolSize < 0)
            throw new IllegalArgumentException();
        int delta = corePoolSize - this.corePoolSize;
        this.corePoolSize = corePoolSize;
        if (workerCountOf(ctl.get()) > corePoolSize)
            interruptIdleWorkers();
        else if (delta > 0) {
            // We don't really know how many new threads are "needed".
            // As a heuristic, prestart enough new workers (up to new
            // core size) to handle the current number of tasks in
            // queue, but stop if queue becomes empty while doing so.
            int k = Math.min(delta, workQueue.size());
            while (k-- > 0 && addWorker(null, true)) {
                if (workQueue.isEmpty())
                    break;
            }
        }
    }

    /**
     * Returns the core number of threads.
     *
     * @return the core number of threads
     * @see #setCorePoolSize
     */
    public int getCorePoolSize() {
        return corePoolSize;
    }

    /**
     * Starts a core thread, causing it to idly wait for work. This
     * overrides the default policy of starting core threads only when
     * new tasks are executed. This method will return {@code false}
     * if all core threads have already been started.
     *
     * @return {@code true} if a thread was started
     */
    public boolean prestartCoreThread() {
        return workerCountOf(ctl.get()) < corePoolSize &&
            addWorker(null, true);
    }

    /**
     * Same as prestartCoreThread except arranges that at least one
     * thread is started even if corePoolSize is 0.
     */
    void ensurePrestart() {
        int wc = workerCountOf(ctl.get());
        if (wc < corePoolSize)
            addWorker(null, true);
        else if (wc == 0)
            addWorker(null, false);
    }

    /**
     * Starts all core threads, causing them to idly wait for work. This
     * overrides the default policy of starting core threads only when
     * new tasks are executed.
     *
     * @return the number of threads started
     */
    public int prestartAllCoreThreads() {
        int n = 0;
        while (addWorker(null, true))
            ++n;
        return n;
    }

    /**
     * Returns true if this pool allows core threads to time out and
     * terminate if no tasks arrive within the keepAlive time, being
     * replaced if needed when new tasks arrive. When true, the same
     * keep-alive policy applying to non-core threads applies also to
     * core threads. When false (the default), core threads are never
     * terminated due to lack of incoming tasks.
     *
     * @return {@code true} if core threads are allowed to time out,
     *         else {@code false}
     *
     * @since 1.6
     */
    public boolean allowsCoreThreadTimeOut() {
        return allowCoreThreadTimeOut;
    }

    /**
     * Sets the policy governing whether core threads may time out and
     * terminate if no tasks arrive within the keep-alive time, being
     * replaced if needed when new tasks arrive. When false, core
     * threads are never terminated due to lack of incoming
     * tasks. When true, the same keep-alive policy applying to
     * non-core threads applies also to core threads. To avoid
     * continual thread replacement, the keep-alive time must be
     * greater than zero when setting {@code true}. This method
     * should in general be called before the pool is actively used.
     *
     * @param value {@code true} if should time out, else {@code false}
     * @throws IllegalArgumentException if value is {@code true}
     *         and the current keep-alive time is not greater than zero
     *
     * @since 1.6
     */
    public void allowCoreThreadTimeOut(boolean value) {
        if (value && keepAliveTime <= 0)
            throw new IllegalArgumentException("Core threads must have nonzero keep alive times");
        if (value != allowCoreThreadTimeOut) {
            allowCoreThreadTimeOut = value;
            if (value)
                interruptIdleWorkers();
        }
    }

    /**
     * Sets the maximum allowed number of threads. This overrides any
     * value set in the constructor. If the new value is smaller than
     * the current value, excess existing threads will be
     * terminated when they next become idle.
     *
     * @param maximumPoolSize the new maximum
     * @throws IllegalArgumentException if the new maximum is
     *         less than or equal to zero, or
     *         less than the {@linkplain #getCorePoolSize core pool size}
     * @see #getMaximumPoolSize
     */
    public void setMaximumPoolSize(int maximumPoolSize) {
        if (maximumPoolSize <= 0 || maximumPoolSize < corePoolSize)
            throw new IllegalArgumentException();
        this.maximumPoolSize = maximumPoolSize;
        if (workerCountOf(ctl.get()) > maximumPoolSize)
            interruptIdleWorkers();
    }

    /**
     * Returns the maximum allowed number of threads.
     *
     * @return the maximum allowed number of threads
     * @see #setMaximumPoolSize
     */
    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    /**
     * Sets the time limit for which threads may remain idle before
     * being terminated.  If there are more than the core number of
     * threads currently in the pool, after waiting this amount of
     * time without processing a task, excess threads will be
     * terminated.  This overrides any value set in the constructor.
     *
     * @param time the time to wait.  A time value of zero will cause
     *        excess threads to terminate immediately after executing tasks.
     * @param unit the time unit of the {@code time} argument
     * @throws IllegalArgumentException if {@code time} less than zero or
     *         if {@code time} is zero and {@code allowsCoreThreadTimeOut}
     * @see #getKeepAliveTime(TimeUnit)
     */
    public void setKeepAliveTime(long time, TimeUnit unit) {
        if (time < 0)
            throw new IllegalArgumentException();
        if (time == 0 && allowsCoreThreadTimeOut())
            throw new IllegalArgumentException("Core threads must have nonzero keep alive times");
        long keepAliveTime = unit.toNanos(time);
        long delta = keepAliveTime - this.keepAliveTime;
        this.keepAliveTime = keepAliveTime;
        if (delta < 0)
            interruptIdleWorkers();
    }

    /**
     * Returns the thread keep-alive time, which is the amount of time
     * that threads in excess of the core pool size may remain
     * idle before being terminated.
     *
     * @param unit the desired time unit of the result
     * @return the time limit
     * @see #setKeepAliveTime(long, TimeUnit)
     */
    public long getKeepAliveTime(TimeUnit unit) {
        return unit.convert(keepAliveTime, TimeUnit.NANOSECONDS);
    }

    /* User-level queue utilities */

    /**
     * Returns the task queue used by this executor. Access to the
     * task queue is intended primarily for debugging and monitoring.
     * This queue may be in active use.  Retrieving the task queue
     * does not prevent queued tasks from executing.
     *
     * @return the task queue
     */
    public BlockingQueue<Runnable> getQueue() {
        return workQueue;
    }

    /**
     * Removes this task from the executor's internal queue if it is
     * present, thus causing it not to be run if it has not already
     * started.
     *
     * <p>This method may be useful as one part of a cancellation
     * scheme.  It may fail to remove tasks that have been converted
     * into other forms before being placed on the internal queue. For
     * example, a task entered using {@code submit} might be
     * converted into a form that maintains {@code Future} status.
     * However, in such cases, method {@link #purge} may be used to
     * remove those Futures that have been cancelled.
     *
     * @param task the task to remove
     * @return {@code true} if the task was removed
     */
    public boolean remove(Runnable task) {
        boolean removed = workQueue.remove(task);
        tryTerminate(); // In case SHUTDOWN and now empty
        return removed;
    }

    /**
     * Tries to remove from the work queue all {@link Future}
     * tasks that have been cancelled. This method can be useful as a
     * storage reclamation operation, that has no other impact on
     * functionality. Cancelled tasks are never executed, but may
     * accumulate in work queues until worker threads can actively
     * remove them. Invoking this method instead tries to remove them now.
     * However, this method may fail to remove tasks in
     * the presence of interference by other threads.
     */
    public void purge() {
        final BlockingQueue<Runnable> q = workQueue;
        try {
            Iterator<Runnable> it = q.iterator();
            while (it.hasNext()) {
                Runnable r = it.next();
                if (r instanceof Future<?> && ((Future<?>)r).isCancelled())
                    it.remove();
            }
        } catch (ConcurrentModificationException fallThrough) {
            // Take slow path if we encounter interference during traversal.
            // Make copy for traversal and call remove for cancelled entries.
            // The slow path is more likely to be O(N*N).
            for (Object r : q.toArray())
                if (r instanceof Future<?> && ((Future<?>)r).isCancelled())
                    q.remove(r);
        }

        tryTerminate(); // In case SHUTDOWN and now empty
    }

    /* Statistics */

    /**
     * Returns the current number of threads in the pool.
     *
     * @return the number of threads
     */
    public int getPoolSize() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // Remove rare and surprising possibility of
            // isTerminated() && getPoolSize() > 0
            return runStateAtLeast(ctl.get(), TIDYING) ? 0
                : workers.size();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate number of threads that are actively
     * executing tasks.
     *
     * @return the number of threads
     */
    public int getActiveCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            int n = 0;
            for (Worker w : workers)
                if (w.isLocked())
                    ++n;
            return n;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the largest number of threads that have ever
     * simultaneously been in the pool.
     *
     * @return the number of threads
     */
    public int getLargestPoolSize() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            return largestPoolSize;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate total number of tasks that have ever been
     * scheduled for execution. Because the states of tasks and
     * threads may change dynamically during computation, the returned
     * value is only an approximation.
     *
     * @return the number of tasks
     */
    public long getTaskCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            long n = completedTaskCount;
            for (Worker w : workers) {
                n += w.completedTasks;
                if (w.isLocked())
                    ++n;
            }
            return n + workQueue.size();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate total number of tasks that have
     * completed execution. Because the states of tasks and threads
     * may change dynamically during computation, the returned value
     * is only an approximation, but one that does not ever decrease
     * across successive calls.
     *
     * @return the number of tasks
     */
    public long getCompletedTaskCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            long n = completedTaskCount;
            for (Worker w : workers)
                n += w.completedTasks;
            return n;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns a string identifying this pool, as well as its state,
     * including indications of run state and estimated worker and
     * task counts.
     *
     * @return a string identifying this pool, as well as its state
     */
    public String toString() {
        long ncompleted;
        int nworkers, nactive;
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            ncompleted = completedTaskCount;
            nactive = 0;
            nworkers = workers.size();
            for (Worker w : workers) {
                ncompleted += w.completedTasks;
                if (w.isLocked())
                    ++nactive;
            }
        } finally {
            mainLock.unlock();
        }
        int c = ctl.get();
        String rs = (runStateLessThan(c, SHUTDOWN) ? "Running" :
                     (runStateAtLeast(c, TERMINATED) ? "Terminated" :
                      "Shutting down"));
        return super.toString() +
            "[" + rs +
            ", pool size = " + nworkers +
            ", active threads = " + nactive +
            ", queued tasks = " + workQueue.size() +
            ", completed tasks = " + ncompleted +
            "]";
    }

    /* Extension hooks */

    /**
     * Method invoked prior to executing the given Runnable in the
     * given thread.  This method is invoked by thread {@code t} that
     * will execute task {@code r}, and may be used to re-initialize
     * ThreadLocals, or to perform logging.
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.beforeExecute} at the end of
     * this method.
     *
     * @param t the thread that will run task {@code r}
     * @param r the task that will be executed
     */
    protected void beforeExecute(Thread t, Runnable r) { }

    /**
     * Method invoked upon completion of execution of the given Runnable.
     * This method is invoked by the thread that executed the task. If
     * non-null, the Throwable is the uncaught {@code RuntimeException}
     * or {@code Error} that caused execution to terminate abruptly.
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.afterExecute} at the
     * beginning of this method.
     *
     * <p><b>Note:</b> When actions are enclosed in tasks (such as
     * {@link FutureTask}) either explicitly or via methods such as
     * {@code submit}, these task objects catch and maintain
     * computational exceptions, and so they do not cause abrupt
     * termination, and the internal exceptions are <em>not</em>
     * passed to this method. If you would like to trap both kinds of
     * failures in this method, you can further probe for such cases,
     * as in this sample subclass that prints either the direct cause
     * or the underlying exception if a task has been aborted:
     *
     *  <pre> {@code
     * class ExtendedExecutor extends ThreadPoolExecutor {
     *   // ...
     *   protected void afterExecute(Runnable r, Throwable t) {
     *     super.afterExecute(r, t);
     *     if (t == null && r instanceof Future<?>) {
     *       try {
     *         Object result = ((Future<?>) r).get();
     *       } catch (CancellationException ce) {
     *           t = ce;
     *       } catch (ExecutionException ee) {
     *           t = ee.getCause();
     *       } catch (InterruptedException ie) {
     *           Thread.currentThread().interrupt(); // ignore/reset
     *       }
     *     }
     *     if (t != null)
     *       System.out.println(t);
     *   }
     * }}</pre>
     *
     * @param r the runnable that has completed
     * @param t the exception that caused termination, or null if
     * execution completed normally
     */
    protected void afterExecute(Runnable r, Throwable t) { }

    /**
     * Method invoked when the Executor has terminated.  Default
     * implementation does nothing. Note: To properly nest multiple
     * overridings, subclasses should generally invoke
     * {@code super.terminated} within this method.
     */
    protected void terminated() { }

    /* Predefined RejectedExecutionHandlers */

    /**
     * A handler for rejected tasks that runs the rejected task
     * directly in the calling thread of the {@code execute} method,
     * unless the executor has been shut down, in which case the task
     * is discarded.
     */
    public static class CallerRunsPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code CallerRunsPolicy}.
         */
        public CallerRunsPolicy() { }

        /**
         * Executes task r in the caller's thread, unless the executor
         * has been shut down, in which case the task is discarded.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                r.run();
            }
        }
    }

    /**
     * A handler for rejected tasks that throws a
     * {@code RejectedExecutionException}.
     */
    public static class AbortPolicy implements RejectedExecutionHandler {
        /**
         * Creates an {@code AbortPolicy}.
         */
        public AbortPolicy() { }

        /**
         * Always throws RejectedExecutionException.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         * @throws RejectedExecutionException always
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            throw new RejectedExecutionException("Task " + r.toString() +
                                                 " rejected from " +
                                                 e.toString());
        }
    }

    /**
     * A handler for rejected tasks that silently discards the
     * rejected task.
     */
    public static class DiscardPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardPolicy}.
         */
        public DiscardPolicy() { }

        /**
         * Does nothing, which has the effect of discarding task r.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        }
    }

    /**
     * A handler for rejected tasks that discards the oldest unhandled
     * request and then retries {@code execute}, unless the executor
     * is shut down, in which case the task is discarded.
     */
    public static class DiscardOldestPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardOldestPolicy} for the given executor.
         */
        public DiscardOldestPolicy() { }

        /**
         * Obtains and ignores the next task that the executor
         * would otherwise execute, if one is immediately available,
         * and then retries execution of task r, unless the executor
         * is shut down, in which case task r is instead discarded.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                e.getQueue().poll();
                e.execute(r);
            }
        }
    }
}
