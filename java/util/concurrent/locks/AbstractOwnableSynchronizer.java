/*
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

/**
 * 一个线程可能专有的同步器.  此类为创建可能涉及所有权概念的锁和相关的同步器提供了基础.
 * * {@code AbstractOwnableSynchronizer}类本身不管理或使用此信息。但是，子类和工具可以使用*适当维护的值来帮助控制和监视访问*并提供诊断信息.
 *
 * @since 1.6
 * @author Doug Lea
 */
public abstract class AbstractOwnableSynchronizer
    implements java.io.Serializable {

    /** 即使所有字段都是瞬态的，也要使用序列号. */
    private static final long serialVersionUID = 3737899427754241961L;

    /**
     * 空构造函数供子类使用.
     */
    protected AbstractOwnableSynchronizer() { }

    /**
     * 独占模式同步的当前所有者.
     */
    private transient Thread exclusiveOwnerThread;

    /**
     * 设置当前拥有独占访问权的线程。 * {@code null}参数表示没有线程拥有访问权限。 *此方法不会强加任何同步或* {@code volatile}字段访问.
     * @param thread the owner thread
     */
    protected final void setExclusiveOwnerThread(Thread thread) {
        exclusiveOwnerThread = thread;
    }

    /**
     * 返回上一次由{@code setExclusiveOwnerThread}，*或{@code null}设置的线程（如果从未设置）.  否则，此方法不会*强制进行任何同步或{@code volatile}字段访问.
     * @return the owner thread
     */
    protected final Thread getExclusiveOwnerThread() {
        return exclusiveOwnerThread;
    }
}
