/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent;

/**
 * {@code Future}表示异步计算的结果.  提供了一些方法来检查计算是否完成, 等待其完成，
 * 并检索计算结果.  计算完成后，只能使用方法{@code get}检索结果，如果需要则阻塞直到完成.
 * 取消是通过 {@code cancel}方法执行的.  提供了其他方法来确定任务是正常完成还是被取消.
 * 计算完成后，无法取消计算.
 * 如果出于可取消性的原因而希望使用{@code Future}而不提供可用的结果，则可以声明
 * {@code Future <？>}形式的类型，并返回{@code null}作为基本任务的结果.
 *
 * <p>
 * <b>用法示例</ b>（请注意，以下所有类都是组成的。）
 * <pre> {@code
 * interface ArchiveSearcher {
 *
 *      String search(String target);
 *
 *  }
 * class App {
 *   ExecutorService executor = ...
 *   ArchiveSearcher searcher = ...
 *   void showSearch(final String target)
 *       throws InterruptedException {
 *     Future<String> future
 *       = executor.submit(new Callable<String>() {
 *         public String call() {
 *             return searcher.search(target);
 *         }});
 *     displayOtherThings(); // do other things while searching
 *     try {
 *       displayText(future.get()); // use future
 *     } catch (ExecutionException ex) { cleanup(); return; }
 *   }
 * }}</pre>
 *
 * {@link FutureTask}类是{@code Future}的实现，该实现实现了{@code Runnable}，因此可以由{@code Executor}执行.
 * 例如，上面的{@code Submit}结构可以替换为:
 *  <pre> {@code
 * FutureTask<String> future =
 *   new FutureTask<String>(new Callable<String>() {
 *     public String call() {
 *       return searcher.search(target);
 *   }});
 * executor.execute(future);}</pre>
 *
 * <p>内存一致性影响：异步计算采取的操作
 * <a href="package-summary.html#MemoryVisibility"> <i> happen-before </ i> </a> *
 * 在相应{@code Future.get之后的操作（）}在另一个线程中.
 *
 * @see FutureTask
 * @see Executor
 * @since 1.5
 * @author Doug Lea
 * @param <V> The result type returned by this Future's {@code get} method
 */
public interface Future<V> {

    /**
     * 尝试取消执行此任务.如果任务已经完成，已经被取消，或者由于某些其他原因而无法取消，则此尝试将失败.
     * 如果成功，并且在调用{@code cancel}时此任务尚未开始，则该任务永远不要运行.  如果任务已经开始，
     * 则{@code mayInterruptIfRunning}参数确定在尝试停止任务时是否应中断执行此任务的线程.
     *
     * <p>此方法返回后，对{@link #isDone}的后续调用将始终返回{@code true}.
     * 如果此方法返回{@code true}，则对{@link #isCancelled} 的后续调用将始终返回{@code true}.
     *
     * @param mayInterruptIfRunning {@code true} if the thread executing this
     * task should be interrupted; otherwise, in-progress tasks are allowed
     * to complete
     * @return {@code false} if the task could not be cancelled,
     * typically because it has already completed normally;
     * {@code true} otherwise
     */
    boolean cancel(boolean mayInterruptIfRunning);

    /**
     * Returns {@code true} if this task was cancelled before it completed
     * normally.
     *
     * @return {@code true} if this task was cancelled before it completed
     */
    boolean isCancelled();

    /**
     * Returns {@code true} if this task completed.
     *
     * Completion may be due to normal termination, an exception, or
     * cancellation -- in all of these cases, this method will return
     * {@code true}.
     *
     * @return {@code true} if this task completed
     */
    boolean isDone();

    /**
     * Waits if necessary for the computation to complete, and then
     * retrieves its result.
     *
     * @return the computed result
     * @throws CancellationException if the computation was cancelled
     * @throws ExecutionException if the computation threw an
     * exception
     * @throws InterruptedException if the current thread was interrupted
     * while waiting
     */
    V get() throws InterruptedException, ExecutionException;

    /**
     * Waits if necessary for at most the given time for the computation
     * to complete, and then retrieves its result, if available.
     *
     * @param timeout the maximum time to wait
     * @param unit the time unit of the timeout argument
     * @return the computed result
     * @throws CancellationException if the computation was cancelled
     * @throws ExecutionException if the computation threw an
     * exception
     * @throws InterruptedException if the current thread was interrupted
     * while waiting
     * @throws TimeoutException if the wait timed out
     */
    V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException;
}
