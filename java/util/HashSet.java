
package java.util;

import java.io.InvalidObjectException;
import sun.misc.SharedSecrets;

/**
 * ①：实现了Serializable接口，表明它支持序列化。
 * ②：实现了Cloneable接口，表明它支持克隆，可以调用超类的clone（）方法进行浅拷贝。
 * ③继承了AbstractSet抽象类，和ArrayList和LinkedList一样，在他们的抽象父类中，都提供了equals（）方法和hashCode（）方法。
 * 它们自身并不实现这两个方法，（但是ArrayList和LinkedList的equals（）实现不同。你可以看我的关于ArrayList这一块的源码解析）
 * 这就意味着诸如和HashSet一样继承自AbstractSet抽象类的TreeSet、LinkedHashSet等，他们只要元素的个数和集合中元素相同，即使
 * 他们是AbstractSet不同的子类，他们equals（）相互比较的后的结果仍然是true。下面给出的代码是JDK中的equals（）代码：
 * 从JDK源码可以看出，底层并没有使用我们常规认为的利用hashcode（）方法求的值进行比较，而是通过调用AbstractCollection的containsAll（）
 * 方法，如果他们中元素完全相同（与顺序无关），则他们的equals（）方法的比较结果就为true。
 *
 *
 *
 * ①HashSet内部通过使用HashMap的键来存储集合中的元素，而且内部的HashMap的所有值
 * 都是null。（因为在为HashSet添加元素的时候，内部HashMap的值都是PRESENT），而PRESENT在实例域的地方直接初始化了，而且不允许改变。
 * ②HashSet对外提供的所有方法内部都是通过HashMap操作完成的，所以，要真正理解HashSet的实现，只需要把HashMap的原理理解即可。我也对HashMap做了分析。传送门：
 * ③所以，如果你对HashMap很熟悉的话，学习HastSet的源码轻而易举！
 *
 *
 * https://blog.csdn.net/m0_37884977/article/details/80562727
 */

public class HashSet<E>
    extends AbstractSet<E>
    implements Set<E>, Cloneable, java.io.Serializable
{

    //序列化ID
    static final long serialVersionUID = -5024744406713321676L;

    //底层使用了HashMap存储数据。
    private transient HashMap<E,Object> map;

    //用来填充底层数据结构HashMap中的value，因为HashSet只用key存储数据。
    private static final Object PRESENT = new Object();

    //其实只是实例化了HashMap
    public HashSet() {
        map = new HashMap<>();
    }


    public HashSet(Collection<? extends E> c) {
        map = new HashMap<>(Math.max((int) (c.size()/.75f) + 1, 16));
        addAll(c);
    }

    public HashSet(int initialCapacity, float loadFactor) {
        map = new HashMap<>(initialCapacity, loadFactor);
    }

    public HashSet(int initialCapacity) {
        map = new HashMap<>(initialCapacity);
    }

    HashSet(int initialCapacity, float loadFactor, boolean dummy) {
        map = new LinkedHashMap<>(initialCapacity, loadFactor);
    }

    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    /**
     * 底层仍然利用了HashMap键进行了元素的添加。
     * 在HashMap的put()方法中，该方法的返回值是对应HashMap中键值对中的值，而值总是PRESENT,
     * 该PRESENT一直都是private static final Object PRESENT = new Object();
     * PRESENT只是初始化了，并不能改变，所以PRESENT的值一直为null。
     * 所以只要插入成功了，put（）方法返回的值总是null。
     */
    public boolean add(E e) {
        return map.put(e, PRESENT)==null;
    }

    /**
     * 该方法底层实现了仍然使用了map的remove（）方法。
     * map的remove（）方法的返回的是被删除键对应的值。（在HashSet的底层HashMap中的所有
     * 键值对的值都是PRESENT）
     */
    public boolean remove(Object o) {
        return map.remove(o)==PRESENT;
    }

    public void clear() {
        map.clear();
    }

    /**
     * Returns a shallow copy of this <tt>HashSet</tt> instance: the elements
     * themselves are not cloned.
     *
     * @return a shallow copy of this set
     */
    @SuppressWarnings("unchecked")
    public Object clone() {
        try {
            HashSet<E> newSet = (HashSet<E>) super.clone();
            newSet.map = (HashMap<E, Object>) map.clone();
            return newSet;
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
    }

    /**
     * 把HashSet写入流中（也就是序列化）
     * @param s
     * @throws java.io.IOException
     */
    private void writeObject(java.io.ObjectOutputStream s)
        throws java.io.IOException {
        // Write out any hidden serialization magic
        s.defaultWriteObject();

        //把HashMap的容量写入流中。
        s.writeInt(map.capacity());
        //把HashMap的装载因子写入流中。
        s.writeFloat(map.loadFactor());
        //把HashMap中键值对的个数写入流中。
        s.writeInt(map.size());

        // 按正确的顺序把集合中的所有元素写入流中。
        for (E e : map.keySet())
            s.writeObject(e);
    }

    /**
     * 从流中读取数据，组装HashSet（反序列化）
     */
    private void readObject(java.io.ObjectInputStream s)
        throws java.io.IOException, ClassNotFoundException {
        // Read in any hidden serialization magic
        s.defaultReadObject();

        // Read capacity and verify non-negative.
        int capacity = s.readInt();
        if (capacity < 0) {
            throw new InvalidObjectException("Illegal capacity: " +
                                             capacity);
        }

        // Read load factor and verify positive and non NaN.
        float loadFactor = s.readFloat();
        if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
            throw new InvalidObjectException("Illegal load factor: " +
                                             loadFactor);
        }

        // Read size and verify non-negative.
        int size = s.readInt();
        if (size < 0) {
            throw new InvalidObjectException("Illegal size: " +
                                             size);
        }
        // Set the capacity according to the size and load factor ensuring that
        // the HashMap is at least 25% full but clamping to maximum capacity.
        capacity = (int) Math.min(size * Math.min(1 / loadFactor, 4.0f),
                HashMap.MAXIMUM_CAPACITY);

        //HashMap中构建哈希桶数组是在第一个元素被添加的时候才构建，所以在构建之前检查它,
        // 调用HashMap.tableSizeFor来计算实际分配的大小,
        // 检查Map.Entry []类，因为它是最接近的公共类型实际创建的内容。
        SharedSecrets.getJavaOISAccess()
                     .checkArray(s, Map.Entry[].class, HashMap.tableSizeFor(capacity));

        //创建HashMap。
        map = (((HashSet<?>)this) instanceof LinkedHashSet ?
               new LinkedHashMap<E,Object>(capacity, loadFactor) :
               new HashMap<E,Object>(capacity, loadFactor));

        // 按写入流中的顺序再把元素依次读取出来放到map中。
        for (int i=0; i<size; i++) {
            @SuppressWarnings("unchecked")
                E e = (E) s.readObject();
            map.put(e, PRESENT);
        }
    }

    public Spliterator<E> spliterator() {
        return new HashMap.KeySpliterator<E,Object>(map, 0, -1, 0, 0);
    }
}
